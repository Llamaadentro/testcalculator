package com.example.llama.calculator;

import android.graphics.Typeface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    TextView helloField; // текстовое поле с приветственной надписью
    TextView resultField; // текстовое поле для вывода результата
    EditText numberField;   // поле для ввода числа
    TextView operationField;    // текстовое поле для вывода знака операции
    Double operand = null;  // операнд операции
    String lastOperation = "="; // последняя операция

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        // получаем все поля по id из activity_main.xml

        helloField = (TextView)findViewById(R.id.helloField);
        resultField =(TextView)findViewById(R.id.resultField);
        numberField = (EditText)findViewById(R.id.numberField);
        operationField = (TextView)findViewById(R.id.operationField);

    }
    // сохранение состояния
    @Override
    protected void onSaveInstanceState(Bundle outState) {
        outState.putString("OPERATION", lastOperation);
        if(operand!=null)
            outState.putDouble("OPERAND", operand);
        super.onSaveInstanceState(outState);
    }
    // получение ранее сохраненного состояния
    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        lastOperation = savedInstanceState.getString("OPERATION");
        operand= savedInstanceState.getDouble("OPERAND");
        resultField.setText(operand.toString());
        operationField.setText(lastOperation);
    }
    // обработка нажатия на числовую кнопку
    public void onNumberClick(View view){

        Button button = (Button)view;

        if(numberField.getText().toString().equals("dividing by zero is forbidden")){
            numberField.setText("");
            numberField.append(button.getText());
        } else {
            numberField.append(button.getText());
        }

        if(lastOperation.equals("=") && operand!=null){
            operand = null;
        }
    }
    //обработка нажатия на кнопку отмены
    public void onDeleteClick(View view){

        Button button = (Button)view;
        String n = numberField.getText().toString();
        if(n.length()>1){
        String n1 = n.substring(0,n.length()-1);
        numberField.setText("");
        numberField.append(n1);
        } else {
            numberField.setText("");
        }

        if(lastOperation.equals("=") && operand!=null){
            operand = null;
        }
    }

    //обработка нажатия на кнопку очистки
    public void onClearClick(View view){

        Button button = (Button)view;
        numberField.setText("");
        resultField.setText("");
        operand = null;

        if(lastOperation.equals("=") && operand!=null){
            operand = null;
        }
    }

    // обработка нажатия на кнопку операции
    public void onOperationClick(View view){
        boolean divzero = false;

        Button button = (Button)view;
        String op = button.getText().toString();
        String number = numberField.getText().toString();
        // если введенно что-нибудь

        if(number.length()>0){
            number = number.replace(',', '.');
            try{
                performOperation(Double.valueOf(number), op);
            }catch (NumberFormatException ex){
                    numberField.setText("");
                } catch (ArithmeticException ex){
                    numberField.setText("dividing by zero is forbidden");
                    divzero = true;
            }
        }

        if (divzero)
        {
            lastOperation = "/";
            operationField.setText(lastOperation);
        } else {
            lastOperation = op;
            operationField.setText(lastOperation);
        }
    }

    private void performOperation(Double number, String operation){

        // если операнд ранее не был установлен (при вводе самой первой операции)
        if(operand ==null){
            operand = number;
        }
        else{
            if(lastOperation.equals("=")){
                lastOperation = operation;
            }
            switch(lastOperation){
                case "=":
                    operand =number;
                    break;
                case "/":
                    if(number==0){
                        throw new ArithmeticException();
                    }
                    else{
                        operand /=number;
                    }
                    break;
                case "*":
                    operand *=number;
                    break;
                case "+":
                    operand +=number;
                    break;
                case "-":
                    operand -=number;
                    break;
            }
        }
        resultField.setText(operand.toString().replace('.', ','));
        numberField.setText("");
    }
}

